console.log("hello")

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const colorText = document.querySelector("#color");
console.log(colorText)
// txtFirstName.addEventListener("keyup", () =>{
// 	spanFullName.innerHTML = `${txtFirstName.value}`;
// })

// txtFirstName.addEventListener("keyup", (event)=>{
// 	console.log(event.target);
// 	console.log(event.target.value);
// })

const fullName = () =>{
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

colorText.addEventListener("change", (event)=>{
	if(event.target.value == "red"){
		spanFullName.style.color = "red";
	}
	else if(event.target.value == "blue"){
		spanFullName.style.color = "blue";
	}
	else if(event.target.value == "green"){
		spanFullName.style.color = "green";
	}

})
/*
Create another addEventListener that will "change the color" of the "spanFullName". Add a "select tag" element with the options/values of red, green, and blue in your index.html.

Check the following links to solve this activity:
	HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
	HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/

